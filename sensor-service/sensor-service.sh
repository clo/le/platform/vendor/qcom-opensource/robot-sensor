#! /bin/sh
###############################################################################
#
# This script is used for starting sensor-service daemon
#
# Copyright (c) 2023 Qualcomm Innovation Center, Inc. All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause-Clear
#
###############################################################################
set -e

case "$1" in
  start)
        echo "Starting sensor-service daemon: "
        start-stop-daemon -S -b -x /sbin/sensor_service
        echo "done"
        ;;
  stop)
        echo "Stopping sensor-service daemon: "
        start-stop-daemon -K -n /sbin/sensor_service
        ;;
  restart)
        $0 stop
        $0 start
        ;;
  *)
        echo "Usage sensor-service.sh { start | stop | restart}"
        exit 1
        ;;
esac

exit 0
