/*
 * Copyright (c) 2023 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 *
 * Not a Contribution.
 *
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifdef SNS_SUPPORT_DIRECT_CHANNEL
#include<string>
/* Taken from sensors_base.h, standard Android sensors */
#define SENSOR_TYPE_ACCELEROMETER                       (1)
#define SENSOR_TYPE_GEOMAGNETIC_FIELD                   (2)
#define SENSOR_TYPE_MAGNETIC_FIELD                      SENSOR_TYPE_GEOMAGNETIC_FIELD
#define SENSOR_TYPE_GYROSCOPE                           (4)
#define SENSOR_TYPE_MAGNETIC_FIELD_UNCALIBRATED         (14)
#define SENSOR_TYPE_GYROSCOPE_UNCALIBRATED              (16)
#define SENSOR_TYPE_ACCELEROMETER_UNCALIBRATED          (35)

/* Taken from sensors_qti.h for dual sensor */
#define QTI_DUAL_SENSOR_TYPE_BASE                       (268369920)
#define QTI_SENSOR_TYPE_ACCELEROMETER                   (QTI_DUAL_SENSOR_TYPE_BASE + SENSOR_TYPE_ACCELEROMETER)
#define QTI_SENSOR_TYPE_ACCELEROMETER_UNCALIBRATED      (QTI_DUAL_SENSOR_TYPE_BASE + SENSOR_TYPE_ACCELEROMETER_UNCALIBRATED)
#define QTI_SENSOR_TYPE_GYROSCOPE                       (QTI_DUAL_SENSOR_TYPE_BASE + SENSOR_TYPE_GYROSCOPE)
#define QTI_SENSOR_TYPE_GYROSCOPE_UNCALIBRATED          (QTI_DUAL_SENSOR_TYPE_BASE + SENSOR_TYPE_GYROSCOPE_UNCALIBRATED)
#define QTI_SENSOR_TYPE_MAGNETIC_FIELD                  (QTI_DUAL_SENSOR_TYPE_BASE + SENSOR_TYPE_MAGNETIC_FIELD)
#define QTI_SENSOR_TYPE_MAGNETIC_FIELD_UNCALIBRATED     (QTI_DUAL_SENSOR_TYPE_BASE + SENSOR_TYPE_MAGNETIC_FIELD_UNCALIBRATED)

/* Definition for packed structures */
#define PACK(x)   x __attribute__((__packed__))

/* Taken from sensors.h */
/**
 * sensor event data
 */

typedef struct sns_sensor_event {
    uint64_t timestamp;
    uint32_t message_id;
    uint32_t event_len;
} sns_sensor_event;

struct sensors {
    std::string sensor_type;
    int calibrated;
    int resampled;
    int sample_rate;
    int channel_type;
    sensors() {}
    sensors(std::string _sensor_type, int _calibrated, int _resampled, int _sample_rate, int _channel_type) {
        sensor_type = _sensor_type;
        calibrated = _calibrated;
        resampled = _resampled;
        sample_rate = _sample_rate;
        channel_type = _channel_type;
    }
};

struct suid_info{
    uint64_t low;
    uint64_t high;
};

typedef enum sns_request_type {
    SNS_GENERIC_SUID = 0,
    SNS_GENERIC_ATTRIBUTES,
    SNS_GENERIC_SAMPLE,
    SNS_ANDROID_MUX_SAMPLE
}sns_request_type;

#endif


